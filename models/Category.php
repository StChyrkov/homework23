<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property int $parentCategoryId
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parentCategoryId'], 'integer'],
            [['title', 'alias'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'alias' => 'Alias',
            'parentCategoryId' => 'Parent Category ID',
        ];
    }

    public function getParentCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'parentCategoryId']);
    }

    public function getCategories()
    {
        return $this->hasMany(Category::class, ['parentCategoryId' => 'id']);
    }

    public function getProducts()
    {
        return $this->hasMany(Product::class, ['categoryId' => 'id']);
    }

    public static function getCategoriesForMenu()
    {
        $parentCategories = Category::find()->where(['parentCategoryId' => null])->all();

        $items = [];

        foreach ($parentCategories as $parentCategory)
        {
            $subCategories = [];

            foreach ($parentCategory->categories as $category)
            {
                $subCategories[] = [
                    'label' => $category->title,
                    'url' => '/products/category/' . $category->alias,
                ];
            }

            $items[] = ['label' => $parentCategory->title, 'items' => $subCategories];
        }

        return $items;
    }
}
