<?php


namespace app\controllers;


use app\models\Page;
use yii\web\Controller;

class PagesController extends Controller
{
    public function actionIndex()
    {
        $pages = Page::find()->all();

        return $this->render(
            'index',
            [
                'title' => 'Дополнительная информация',
                'pages' => $pages,
            ]
        );
    }

    public function actionView($alias)
    {
        $page = Page::find()->where(['alias' => $alias])->one();
        return $this->render('view', ['page' => $page]);
    }
}