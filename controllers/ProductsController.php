<?php


namespace app\controllers;


use app\models\Category;
use app\models\Product;
use yii\web\Controller;

class ProductsController extends Controller
{
    public function actionIndex()
    {
        $products = Product::find()->all();

        return $this->render(
            'index',
            [
                'title' => 'Товары нашего интернет-магазина',
                'products' => $products,
            ]
        );
    }

    public function actionView($id)
    {
        $product = Product::findOne($id);
        return $this->render('view', ['product' => $product]);
    }

    public function actionCategory($alias)
    {
        $category = Category::find()->where(['alias' => $alias])->one();;
        return $this->render('category', ['category' => $category]);
    }
}