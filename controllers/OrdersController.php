<?php


namespace app\controllers;


use app\models\Order;
use yii\web\Controller;

class OrdersController extends Controller
{
    public function actionIndex()
    {
        $orders = Order::find()->all();

        return $this->render(
            'index',
            [
                'title' => 'Заказы',
                'orders' => $orders,
            ]
        );
    }
}