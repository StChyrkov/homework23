<?php

use yii\db\Migration;

/**
 * Class m190822_154727_create_catogories_table_and_add_parent_catogories
 */
class m190822_154727_create_catogories_table_and_add_parent_catogories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('categories', [
            'id' => $this->primaryKey(),
            'title' => $this->string(200),
            'alias' => $this->string(200),
            'parentCategoryId' => $this->integer(),
        ]);

        $this->insert('categories', [
            'title' => 'Товары для собак',
            'alias' => 'dogs_products',
        ]);

        $this->insert('categories', [
            'title' => 'Товары для кошек',
            'alias' => 'cats_products',
        ]);

        $this->insert('categories', [
            'title' => 'Товары для птиц',
            'alias' => 'birds_products',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('categories');
    }

}
