<?php

use yii\db\Migration;

/**
 * Class m190822_155757_add_categories
 */
class m190822_155757_add_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('categories', [
            'title' => 'Корма для собак',
            'alias' => 'dogs_foods',
            'parentCategoryId' => 1,
        ]);

        $this->insert('categories', [
            'title' => 'Игрушки для собак',
            'alias' => 'dogs_toys',
            'parentCategoryId' => 1,
        ]);

        $this->insert('categories', [
            'title' => 'Аксессуары для собак',
            'alias' => 'dogs_accessories',
            'parentCategoryId' => 1,
        ]);

        $this->insert('categories', [
            'title' => 'Корма для кошек',
            'alias' => 'cats_foods',
            'parentCategoryId' => 2,
        ]);

        $this->insert('categories', [
            'title' => 'Игрушки для кошек',
            'alias' => 'cats_toys',
            'parentCategoryId' => 2,
        ]);

        $this->insert('categories', [
            'title' => 'Наполнители для туалетов',
            'alias' => 'cats_litters',
            'parentCategoryId' => 2,
        ]);

        $this->insert('categories', [
            'title' => 'Корма для птиц',
            'alias' => 'birds_foods',
            'parentCategoryId' => 3,
        ]);

        $this->insert('categories', [
            'title' => 'Лакомства для птиц',
            'alias' => 'birds_treats',
            'parentCategoryId' => 3,
        ]);

        $this->insert('categories', [
            'title' => 'Клетки для птиц',
            'alias' => 'birds_cages',
            'parentCategoryId' => 3,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('categories');
    }


}
