<?php

use yii\db\Migration;

/**
 * Class m190802_170237_add_orders
 */
class m190802_170237_add_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('orders', [
            'customer_name' => 'Василий Галоян',
            'email' => 'gala1988@ukr.net',
            'phone' => '80635951610',
            'feedback' => 'Постоянно заказываю здесь корм для своего кота. Мурзик очень благодарен вам за приятные цены
            и быструю доставку!',
        ]);

        $this->insert('orders', [
            'customer_name' => 'Анжела Мясоед',
            'email' => 'vegan_od@gmail.com',
            'phone' => '380669981919',
            'feedback' => 'В Лысой Собаке я покупаю все необходиомое для своих 3-х собак. Уже накопила на скидку 10%
            и теперь экономлю еще больше! Спасибо персоналу магазина за попомщь в выборе кормов.',
        ]);

        $this->insert('orders', [
            'customer_name' => 'Виталий Косоворотов',
            'email' => 'vitalya.kos@mail.ru',
            'phone' => '380501234400',
            'feedback' => 'Вчера сделал первый заказ и уже сегодня получил посылку с кормом для своего попугайчика. 
            Теперь Кеше будет, что поесть!',
        ]);

        $this->insert('orders', [
            'customer_name' => 'Максим Обрывко',
            'email' => 'maximal_max@ya.ru',
            'phone' => '380934499090',
            'feedback' => 'Закзал себе переноску для кота. Выбрал этот магазин из-за цены. Переноска стоит на 100 гривен
            дешевле, чем такая же в магазине за углом.',
        ]);

        $this->insert('orders', [
            'customer_name' => 'Степан Кукареков',
            'email' => 'dubstepfan@gmail.com',
            'phone' => '380997881209',
            'feedback' => 'Спасибо вам, Лысая Собака! Уже несколько лет покупаю товары для совего хомячка по лучшим
            ценам только здесь!',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('orders');
    }

}
