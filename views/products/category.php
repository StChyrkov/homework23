<?php
/**
 * @var \app\models\Product $product
 */
$this->title = 'Интернет-магазин Лысая Собака: товары';
?>

<h1><?=$category->title?></h1>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Код товара</th>
        <th scope="col">Название</th>
        <th scope="col">Цена</th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($category->products as $product) : ?>
        <tr>
            <td><?=$product->id?></td>
            <td><?=$product->title?></td>
            <td><?=$product->price?> грн</td>
            <td><a href="/products/category/<?=$category->alias?>/<?=$product->id ?>" class="btn btn-primary">Подробнее</a></td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>