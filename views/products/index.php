<?php
/**
 * @var \app\models\Product[] $products
 */
$this->title = 'Интернет-магазин Лысая Собака: товары';
?>

<h1><?=$title?></h1>
<table class="table">
    <thead>
        <tr>
            <th scope="col">Код товара</th>
            <th scope="col">Название</th>
            <th scope="col">Цена</th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($products as $product) : ?>
        <tr>
            <td><?=$product->id?></td>
            <td><?=$product->title?></td>
            <td><?=$product->price?> грн</td>
            <td><a href="/products/view/<?=$product->id ?>" class="btn btn-primary">Подробнее</a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>


