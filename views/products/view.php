<?php
/**
 * @var \app\models\Product $product
 */
$this->title = 'Интернет-магазин Лысая Собака: товары';
?>

<h1><?=$product->title?></h1>
<p><b>Код товара: </b><?=$product->id?></p>
<p><b>Оригинальное название: </b><?=$product->alias?></p>
<p><b>Цена: </b><?=$product->price?> грн.</p>
<p><?=$product->description?></p>
<a href="/products/category/<?=$product->category->alias ?>" class="btn btn-primary">Другие товары этой категории</a>
