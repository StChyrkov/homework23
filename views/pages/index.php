<?php
/**
 * @var \app\models\Page[] $pages
 */
$this->title = 'Интернет-магазин Лысая Собака: информация';
?>

<h1><?=$title?></h1>
<ul>
    <?php foreach ($pages as $page) : ?>
    <li><a href="/pages/view/<?=$page->alias?>"><?=$page->title?></a></li>
    <?php endforeach; ?>
</ul>
