<?php
/**
 * @var \app\models\Page[] $page
 */
$this->title = 'Интернет-магазин Лысая Собака: информация';
?>

<h1><?=$page->title?></h1>
<p><b><?=$page->intro?></b></p>
<p><?=$page->content?></p>
