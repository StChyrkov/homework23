<?php

/* @var $this yii\web\View */

$this->title = 'Интернет-магазин Лысая Собака';
?>
<div class="site-index">
    <h1>Добро пожаловать в интернет-магазин Лысая Собака!</h1>
    <h3>Только у нас вы сможете заказать лучшие товары для ваших питомцев по доступным ценам!</h3>
</div>
