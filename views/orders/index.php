<?php
/**
 * @var \app\models\Order[] $orders
 */

$this->title = 'Интернет-магазин Лысая Собака: заказы';
?>

<h1><?=$title?></h1>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Номер заказа</th>
        <th scope="col">Имя клиента</th>
        <th scope="col">Email</th>
        <th scope="col">Телефон</th>
        <th scope="col">Комментарий</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($orders as $order) : ?>
        <tr>
            <td><?=$order->id?></td>
            <td><?=$order->customer_name?></td>
            <td><?=$order->email?></td>
            <td><?=$order->phone?></td>
            <td><?=$order->feedback?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
